FROM alpine

ARG docker_version=17.04.0
ARG compose_version=1.12.0

RUN apk update && \
    apk add --no-cache \
    curl

RUN curl -L https://get.docker.com/builds/Linux/x86_64/docker-${docker_version}-ce.tgz > docker.tgz && \
    tar xvfz docker.tgz && \
    mv docker/docker /usr/bin/docker && \
    rm docker.tgz && \
    rm -rf docker

RUN apk add --no-cache curl openssl ca-certificates \
    && curl -L https://github.com/docker/compose/releases/download/${compose_version}/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose \
    && chmod +x /usr/local/bin/docker-compose \
    && wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://raw.githubusercontent.com/sgerrand/alpine-pkg-glibc/master/sgerrand.rsa.pub \
    && wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.23-r3/glibc-2.23-r3.apk \
    && apk add --no-cache glibc-2.23-r3.apk && rm glibc-2.23-r3.apk \
    && ln -s /lib/libz.so.1 /usr/glibc-compat/lib/ \
    && ln -s /lib/libc.musl-x86_64.so.1 /usr/glibc-compat/lib

ENTRYPOINT ["/usr/local/bin/docker-compose"]
CMD ["help"]
